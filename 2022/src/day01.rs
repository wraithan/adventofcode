pub fn solve_puzzle_part_1(input: &str) -> Result<u32, String> {
    let entries = input.lines().try_fold(vec![vec![]], |mut acc, l| {
        if l.is_empty() {
            acc.push(vec![]);
        } else {
            let value = l
                .parse::<u32>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))?;
            let current = acc.last_mut().unwrap();
            current.push(value);
        }
        Ok::<_, String>(acc)
    })?;
    let answer = entries
        .into_iter()
        .map(|set| set.into_iter().sum())
        .max()
        .unwrap();
    Ok(answer)
}

pub fn solve_puzzle_part_2(input: &str) -> Result<u32, String> {
    let entries = input.lines().try_fold(vec![vec![]], |mut acc, l| {
        if l.is_empty() {
            acc.push(vec![]);
        } else {
            let value = l
                .parse::<u32>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))?;
            let current = acc.last_mut().unwrap();
            current.push(value);
        }
        Ok::<_, String>(acc)
    })?;
    let mut totals: Vec<u32> = entries
        .into_iter()
        .map(|set| set.into_iter().sum())
        .collect();

    totals.sort();
    let answer = totals.into_iter().rev().take(3).sum();
    Ok(answer)
}

#[cfg(test)]
mod test {
    const INPUT: &str = "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";

    mod part_1 {
        use super::{super::solve_puzzle_part_1, INPUT};

        #[test]
        fn example_01() {
            let expected = 24000;
            let result = solve_puzzle_part_1(INPUT).unwrap();
            assert_eq!(result, expected)
        }
    }

    #[cfg(test)]
    mod part_2 {
        use super::{super::solve_puzzle_part_2, INPUT};

        #[test]
        fn example_01() {
            let expected = 45000;
            let result = solve_puzzle_part_2(INPUT).unwrap();
            assert_eq!(result, expected)
        }
    }
}
