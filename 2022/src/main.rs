mod day01;
mod day02;

use std::{
    fmt::Display,
    io::{self, Write},
    time::{Duration, Instant},
};
use termcolor::{BufferWriter, Color, ColorChoice, ColorSpec, WriteColor};

macro_rules! expand_day {
    ($day_name:ident, $display_name:expr, $expected:expr) => {
        // Load input file
        let (input, load_time) = time_func(|| {
            let day_name = stringify!($day_name);
            let mut input_path = std::path::PathBuf::from(env!("CARGO_MANIFEST_DIR"));
            input_path.push("input");
            input_path.push(&format!("{}.txt", day_name));
            std::fs::read_to_string(input_path).expect("input text")
        });

        println!(
            "{} (load time: {}.{:06}s)",
            $display_name,
            load_time.as_secs(),
            load_time.subsec_micros(),
        );

        // Solve puzzles
        let (part1_answer, part1_time) = time_func(|| $day_name::solve_puzzle_part_1(&input));

        let (part2_answer, part2_time) = time_func(|| $day_name::solve_puzzle_part_2(&input));

        print_result(part1_answer, $expected.0, part1_time, 1).expect("failed to print result");
        print_result(part2_answer, $expected.1, part2_time, 2).expect("failed to print result");
    };
}

fn time_func<F, T>(func: F) -> (T, Duration)
where
    F: FnOnce() -> T,
    T: Sized,
{
    let start = Instant::now();
    (func(), start.elapsed())
}

fn print_result<T: Sized + Eq + Display>(
    answer: Result<T, String>,
    expected: T,
    time: Duration,
    number: usize,
) -> io::Result<()> {
    let mut right_color = ColorSpec::new();
    right_color.set_fg(Some(Color::Green));
    let mut wrong_color = ColorSpec::new();
    wrong_color.set_fg(Some(Color::Red));

    let mut error_output = None;
    let output_writer = BufferWriter::stdout(ColorChoice::Auto);
    let error_writer = BufferWriter::stderr(ColorChoice::Auto);
    let mut output = output_writer.buffer();
    let mut error = error_writer.buffer();

    let (color, result, answer) = match answer {
        Ok(answer) => {
            if expected == answer {
                (&right_color, "✓", Some(answer))
            } else {
                (&wrong_color, "X", Some(answer))
            }
        }
        Err(e) => {
            error_output = Some(format!("Error in puzzle: {}", e));
            (&wrong_color, "🔥", None)
        }
    };
    output.set_color(color)?;
    write!(
        output,
        "  Part {} ({}.{:06}s) ({}): ",
        number,
        time.as_secs(),
        time.subsec_micros(),
        result
    )?;
    if let Some(answer) = answer {
        write!(output, "{}", answer)?;
    }
    writeln!(output)?;
    output_writer.print(&output)?;
    output.clear();
    output.reset()?;
    output_writer.print(&output)?;

    if let Some(error_output) = error_output {
        writeln!(error, "  {}", error_output)?;
        error_writer.print(&error)?;
    };
    Ok(())
}

fn main() {
    let (_, total) = time_func(|| {
        expand_day!(day01, "Day 01", (72_070, 211_805));
        expand_day!(day02, "Day 02", (14_069, 12_411));
    });
    println!(
        "Total time: {}.{:06}s",
        total.as_secs(),
        total.subsec_micros()
    );
}
