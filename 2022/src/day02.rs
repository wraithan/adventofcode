use std::{cmp::Ordering, str::FromStr};

#[derive(PartialEq, Eq, Clone, Copy)]
enum Choice {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

impl PartialOrd for Choice {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(match self {
            Choice::Rock => match other {
                Choice::Rock => Ordering::Equal,
                Choice::Paper => Ordering::Less,
                Choice::Scissors => Ordering::Greater,
            },
            Choice::Paper => match other {
                Choice::Rock => Ordering::Greater,
                Choice::Paper => Ordering::Equal,
                Choice::Scissors => Ordering::Less,
            },
            Choice::Scissors => match other {
                Choice::Rock => Ordering::Less,
                Choice::Paper => Ordering::Greater,
                Choice::Scissors => Ordering::Equal,
            },
        })
    }
}

impl FromStr for Choice {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "A" | "X" => Ok(Choice::Rock),
            "B" | "Y" => Ok(Choice::Paper),
            "C" | "Z" => Ok(Choice::Scissors),
            _ => Err(format!("Unknown choice {}", s)),
        }
    }
}

enum Strategy {
    Lose,
    Draw,
    Win,
}

impl Strategy {
    fn get_move(self, choice: &Choice) -> Choice {
        match self {
            Strategy::Lose => match choice {
                Choice::Rock => Choice::Scissors,
                Choice::Paper => Choice::Rock,
                Choice::Scissors => Choice::Paper,
            },
            Strategy::Draw => *choice,
            Strategy::Win => match choice {
                Choice::Rock => Choice::Paper,
                Choice::Paper => Choice::Scissors,
                Choice::Scissors => Choice::Rock,
            },
        }
    }
}

impl FromStr for Strategy {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "X" => Ok(Strategy::Lose),
            "Y" => Ok(Strategy::Draw),
            "Z" => Ok(Strategy::Win),
            _ => Err(format!("Unknown strategy {}", s)),
        }
    }
}

pub fn solve_puzzle_part_1(input: &str) -> Result<u32, String> {
    let games = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            let mut parts = l.split(' ');
            let first = parts
                .next()
                .ok_or_else(|| "Line didn't have first position?".to_string())?;
            let second = parts
                .next()
                .ok_or_else(|| "Line didn't have second position?".to_string())?;
            let value1 = first
                .parse::<Choice>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))?;
            let value2 = second
                .parse::<Choice>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))?;

            Ok((value1, value2))
        })
        .collect::<Result<Vec<(Choice, Choice)>, String>>()?;
    let mut total = 0;
    for game in games {
        total += game.1 as u32;
        if game.0 < game.1 {
            total += 6;
        } else if game.0 == game.1 {
            total += 3;
        }
    }
    Ok(total)
}

pub fn solve_puzzle_part_2(input: &str) -> Result<u32, String> {
    let games = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            let mut parts = l.split(' ');
            let first = parts
                .next()
                .ok_or_else(|| "Line didn't have first position?".to_string())?;
            let second = parts
                .next()
                .ok_or_else(|| "Line didn't have second position?".to_string())?;
            let value1 = first
                .parse::<Choice>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))?;
            let value2 = second
                .parse::<Strategy>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))?;
            let response = value2.get_move(&value1);

            Ok((value1, response))
        })
        .collect::<Result<Vec<(Choice, Choice)>, String>>()?;
    let mut total = 0;
    for game in games {
        total += game.1 as u32;
        if game.0 < game.1 {
            total += 6;
        } else if game.0 == game.1 {
            total += 3;
        }
    }
    Ok(total)
}

#[cfg(test)]
mod test {
    const INPUT: &str = "A Y
B X
C Z";

    mod part_1 {
        use super::{super::solve_puzzle_part_1, INPUT};

        #[test]
        fn example_01() {
            let expected = 8;
            let result = solve_puzzle_part_1("A Y").unwrap();
            assert_eq!(result, expected)
        }

        #[test]
        fn example_02() {
            let expected = 1;
            let result = solve_puzzle_part_1("B X").unwrap();
            assert_eq!(result, expected)
        }

        #[test]
        fn example_03() {
            let expected = 6;
            let result = solve_puzzle_part_1("C Z").unwrap();
            assert_eq!(result, expected)
        }

        #[test]
        fn example_together() {
            let expected = 15;
            let result = solve_puzzle_part_1(INPUT).unwrap();
            assert_eq!(result, expected)
        }
    }

    #[cfg(test)]
    mod part_2 {
        use super::{super::solve_puzzle_part_2, INPUT};

        #[test]
        fn example_01() {
            let expected = 12;
            let result = solve_puzzle_part_2(INPUT).unwrap();
            assert_eq!(result, expected)
        }
    }
}
