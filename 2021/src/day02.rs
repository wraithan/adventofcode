pub fn solve_puzzle_part_1(input: &str) -> Result<u32, String> {
    let entries = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            let mut parts = l.split(' ');
            let direction = parts
                .next()
                .ok_or_else(|| format!("Line had no direction? {}", l))?;
            let amount = parts
                .next()
                .ok_or_else(|| format!("Line had no amount? {}", l))?
                .parse::<u32>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))?;
            Ok((direction, amount))
        })
        .collect::<Result<Vec<(&str, u32)>, String>>()?;

    let mut horizontal = 0;
    let mut depth = 0;

    for (direction, amount) in entries {
        match direction {
            "forward" => horizontal += amount,
            "down" => depth += amount,
            "up" => depth -= amount,
            _ => return Err(format!("Unknown direction {}", direction)),
        };
    }

    Ok(horizontal * depth)
}

pub fn solve_puzzle_part_2(input: &str) -> Result<u32, String> {
    let entries = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            let mut parts = l.split(' ');
            let direction = parts
                .next()
                .ok_or_else(|| format!("Line had no direction? {}", l))?;
            let amount = parts
                .next()
                .ok_or_else(|| format!("Line had no amount? {}", l))?
                .parse::<u32>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))?;
            Ok((direction, amount))
        })
        .collect::<Result<Vec<(&str, u32)>, String>>()?;

    let mut horizontal = 0;
    let mut depth = 0;
    let mut aim = 0;

    for (direction, amount) in entries {
        match direction {
            "forward" => {
                horizontal += amount;
                depth += aim * amount;
            }
            "down" => aim += amount,
            "up" => aim -= amount,
            _ => return Err(format!("Unknown direction {}", direction)),
        };
    }

    Ok(horizontal * depth)
}

#[cfg(test)]
mod test {
    const INPUT: &str = "forward 5\ndown 5\nforward 8\nup 3\ndown 8\nforward 2\n";

    mod part_1 {
        use super::{super::solve_puzzle_part_1, INPUT};

        #[test]
        fn example_01() {
            let expected = 150;
            let result = solve_puzzle_part_1(INPUT).unwrap();
            assert_eq!(result, expected)
        }
    }

    #[cfg(test)]
    mod part_2 {
        use super::{super::solve_puzzle_part_2, INPUT};

        #[test]
        fn example_01() {
            let expected = 900;
            let result = solve_puzzle_part_2(INPUT).unwrap();
            assert_eq!(result, expected)
        }
    }
}
