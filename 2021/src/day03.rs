pub fn solve_puzzle_part_1(input: &str) -> Result<u32, String> {
    let entries = input
        .lines()
        .filter(|l| !l.is_empty())
        .collect::<Vec<&str>>();

    let mut counts: Vec<(u32, u32)> = vec![];
    {
        let first = entries
            .first()
            .ok_or_else(|| "No first entry?".to_string())?;
        counts.resize(first.len(), (0, 0));
    }

    for entry in entries {
        for (i, val) in entry.chars().enumerate() {
            match val {
                '0' => counts[i].0 += 1,
                '1' => counts[i].1 += 1,
                _ => return Err(format!("Unexpected input in number: {}", val)),
            }
        }
    }

    let (gamma, epsilon) = {
        let mut raw_gamma = String::new();
        let mut raw_epsilon = String::new();
        for count in &counts {
            if count.0 > count.1 {
                raw_gamma.push('0');
                raw_epsilon.push('1');
            } else {
                raw_gamma.push('1');
                raw_epsilon.push('0');
            }
        }
        (
            u32::from_str_radix(&raw_gamma, 2)
                .map_err(|err| format!("Error parsing gamma: {:#?}", err))?,
            u32::from_str_radix(&raw_epsilon, 2)
                .map_err(|err| format!("Error parsing epsilon: {:#?}", err))?,
        )
    };

    Ok(gamma * epsilon)
}

pub fn solve_puzzle_part_2(input: &str) -> Result<u32, String> {
    let entries = input
        .lines()
        .filter(|l| !l.is_empty())
        .collect::<Vec<&str>>();

    let oxygen = get_life_support_value(entries.clone(), LifeSupport::Oxygen)?;
    let c02 = get_life_support_value(entries, LifeSupport::C02)?;

    Ok(oxygen * c02)
}

enum LifeSupport {
    Oxygen,
    C02,
}

fn get_life_support_value(mut entries: Vec<&str>, system: LifeSupport) -> Result<u32, String> {
    let length = {
        entries
            .first()
            .ok_or_else(|| "No first entry?".to_string())?
            .len()
    };
    for position in 0..length {
        let rates = get_rates_for_position(&entries, position)?;
        let keep = match system {
            LifeSupport::Oxygen => {
                if rates.0 <= rates.1 {
                    '1'
                } else {
                    '0'
                }
            }
            LifeSupport::C02 => {
                if rates.0 <= rates.1 {
                    '0'
                } else {
                    '1'
                }
            }
        };
        entries = entries
            .into_iter()
            .filter(|v| v.chars().nth(position) == Some(keep))
            .collect();
        if entries.len() == 1 {
            break;
        }
    }
    u32::from_str_radix(entries[0], 2).map_err(|err| format!("Error parsing value: {:#?}", err))
}

fn get_rates_for_position(entries: &[&str], position: usize) -> Result<(u32, u32), String> {
    let mut counts = (0, 0);
    for entry in entries {
        match entry.chars().nth(position) {
            Some('0') => counts.0 += 1,
            Some('1') => counts.1 += 1,
            _ => {
                return Err(format!(
                    "Unexpected input in number, or position out of range: {}",
                    entry
                ))
            }
        }
    }
    Ok(counts)
}

#[cfg(test)]
mod test {
    const INPUT: &str =
        "00100\n11110\n10110\n10111\n10101\n01111\n00111\n11100\n10000\n11001\n00010\n01010\n";

    mod part_1 {
        use super::{super::solve_puzzle_part_1, INPUT};

        #[test]
        fn example_01() {
            let expected = 198;
            let result = solve_puzzle_part_1(INPUT).unwrap();
            assert_eq!(result, expected)
        }
    }

    #[cfg(test)]
    mod part_2 {
        use super::{
            super::{
                get_life_support_value, get_rates_for_position, solve_puzzle_part_2, LifeSupport,
            },
            INPUT,
        };

        #[test]
        fn example_01() {
            let expected = 230;
            let result = solve_puzzle_part_2(INPUT).unwrap();
            assert_eq!(result, expected)
        }

        #[test]
        fn get_rates_for_position_from_example() {
            let entries = INPUT
                .lines()
                .filter(|l| !l.is_empty())
                .collect::<Vec<&str>>();
            let expected = (5, 7);
            let result = get_rates_for_position(&entries, 0).unwrap();
            assert_eq!(result, expected)
        }

        #[test]
        fn get_life_support_value_oxygen() {
            let entries = INPUT
                .lines()
                .filter(|l| !l.is_empty())
                .collect::<Vec<&str>>();
            let expected = 23;
            let result = get_life_support_value(entries, LifeSupport::Oxygen).unwrap();
            assert_eq!(result, expected)
        }

        #[test]
        fn get_life_support_value_c02() {
            let entries = INPUT
                .lines()
                .filter(|l| !l.is_empty())
                .collect::<Vec<&str>>();
            let expected = 10;
            let result = get_life_support_value(entries, LifeSupport::C02).unwrap();
            assert_eq!(result, expected)
        }
    }
}
