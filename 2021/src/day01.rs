pub fn solve_puzzle_part_1(input: &str) -> Result<u32, String> {
    let entries = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            l.parse::<u32>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))
        })
        .collect::<Result<Vec<u32>, String>>()?;

    let mut last = entries.first().ok_or("No entries?")?;
    let mut increases = 0;
    for entry in &entries {
        if entry > last {
            increases += 1;
        }
        last = entry;
    }

    Ok(increases)
}

pub fn solve_puzzle_part_2(input: &str) -> Result<u32, String> {
    let entries = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            l.parse::<u32>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))
        })
        .collect::<Result<Vec<u32>, String>>()?;

    let mut last = 0;
    let mut increases = 0;
    for entry in entries.windows(3) {
        let val = entry[0] + entry[1] + entry[2];
        if last != 0 && val > last {
            increases += 1;
        }
        last = val;
    }

    Ok(increases)
}

#[cfg(test)]
mod test {
    const INPUT: &str = "199\n200\n208\n210\n200\n207\n240\n269\n260\n263\n";

    mod part_1 {
        use super::{super::solve_puzzle_part_1, INPUT};

        #[test]
        fn example_01() {
            let expected = 7;
            let result = solve_puzzle_part_1(INPUT).unwrap();
            assert_eq!(result, expected)
        }
    }

    #[cfg(test)]
    mod part_2 {
        use super::{super::solve_puzzle_part_2, INPUT};

        #[test]
        fn example_01() {
            let expected = 5;
            let result = solve_puzzle_part_2(INPUT).unwrap();
            assert_eq!(result, expected)
        }
    }
}
