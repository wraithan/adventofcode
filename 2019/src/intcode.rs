use std::collections::{HashMap, VecDeque};
use std::str::FromStr;

#[derive(Clone, Debug)]
pub struct IntcodeMachine {
    program: Vec<i64>,
    cursor: i64,
    relative_base: i64,
    input: VecDeque<i64>,
    output: VecDeque<i64>,
    last_output: Option<i64>,
    state: MachineState,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum MachineState {
    Initialized,
    Running,
    IOWait,
    Halt,
}

#[derive(Debug, Eq, PartialEq)]
enum Operation {
    Add {
        lhs: Parameter,
        rhs: Parameter,
        dest: Parameter,
    },
    Multiply {
        lhs: Parameter,
        rhs: Parameter,
        dest: Parameter,
    },
    Input {
        dest: Parameter,
    },
    Output {
        src: Parameter,
    },
    JumpTrue {
        arg: Parameter,
        dest: Parameter,
    },
    JumpFalse {
        arg: Parameter,
        dest: Parameter,
    },
    LessThan {
        lhs: Parameter,
        rhs: Parameter,
        dest: Parameter,
    },
    Equals {
        lhs: Parameter,
        rhs: Parameter,
        dest: Parameter,
    },
    AdjustRelativeBase {
        arg: Parameter,
    },
    Stop,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Parameter {
    Position(i64),
    Immediate(i64),
    Relative(i64),
}

impl Parameter {
    fn new(param_type: i64, value: i64) -> Self {
        match param_type {
            0 => Parameter::Position(value),
            1 => Parameter::Immediate(value),
            2 => Parameter::Relative(value),
            _ => unimplemented!("New paramter type"),
        }
    }
}

pub fn controller(
    mut tree: Vec<(&str, IntcodeMachine, &str)>,
    output: &str,
) -> Result<i64, String> {
    let mut machines: HashMap<String, (IntcodeMachine, String)> = HashMap::default();
    let mut data_exchanger: HashMap<String, VecDeque<i64>> = HashMap::default();
    for (name, machine, target) in tree.drain(..) {
        machines.insert(name.to_string(), (machine, target.to_string()));
        data_exchanger.insert(name.to_string(), VecDeque::default());
    }
    while machines.values().any(|m| !m.0.is_halted()) {
        for (machine, target_name) in machines.values_mut() {
            if !machine.is_halted() {
                machine.run(&[])?;
                while let Some(output) = machine.get_output() {
                    data_exchanger
                        .get_mut(target_name)
                        .unwrap()
                        .push_back(output);
                }
            }
        }
        for (name, (machine, _)) in machines.iter_mut() {
            let data = data_exchanger.get_mut(name).unwrap();
            for val in data.drain(..) {
                machine.add_input(val);
            }
        }
    }
    machines
        .get(output)
        .unwrap()
        .0
        .get_last_output()
        .ok_or_else(|| "no ouput?".to_string())
}

impl IntcodeMachine {
    pub fn with_input(mut self, input: &[i64]) -> Self {
        for val in input {
            self.input.push_back(*val);
        }
        self
    }

    pub fn reset(&mut self) {
        self.state = MachineState::Initialized;
        self.cursor = 0;
    }

    fn get(&self, param: Parameter) -> Option<i64> {
        use Parameter::*;

        match param {
            Position(index) => self.get_position(index),
            Immediate(value) => Some(value),
            Relative(offset) => self.get_position(self.relative_base + offset),
        }
    }

    fn get_mut(&mut self, param: Parameter) -> Option<&mut i64> {
        use Parameter::*;
        let index = match param {
            Position(index) => index,
            Relative(offset) => self.relative_base + offset,
            Immediate(_) => unimplemented!(
                "Parameters that an instruction writes to will never be in immediate mode."
            ),
        };
        if index >= 0 {
            let index = index as usize;
            if index >= self.program.len() {
                self.program.resize(index + 1, 0);
            }
            self.program.get_mut(index)
        } else {
            None
        }
    }

    pub fn get_position(&self, index: i64) -> Option<i64> {
        if index >= 0 {
            self.program.get(index as usize).copied().or(Some(0))
        } else {
            None
        }
    }

    pub fn get_program(&self) -> &[i64] {
        &self.program
    }

    pub fn get_program_mut(&mut self) -> &mut [i64] {
        &mut self.program
    }

    pub fn get_last_output(&self) -> Option<i64> {
        self.last_output
    }

    pub fn get_input(&mut self) -> Option<i64> {
        self.input.pop_front()
    }

    pub fn get_output(&mut self) -> Option<i64> {
        self.output.pop_front()
    }

    fn add_input(&mut self, val: i64) {
        self.input.push_back(val)
    }

    pub fn is_halted(&self) -> bool {
        self.state == MachineState::Halt
    }

    pub fn run(&mut self, input: &[i64]) -> Result<(), String> {
        for val in input {
            self.input.push_back(*val);
        }
        self.state = MachineState::Running;
        // eprintln!("{:?}", self);
        while MachineState::Running == self.state {
            // eprintln!("Program: {:?}", self.program);
            self.execute_current_op()?;
        }
        // eprintln!("{:?}", self);
        Ok(())
    }

    fn current_op(&self) -> Result<Operation, String> {
        let opcode = self
            .get_position(self.cursor)
            .ok_or_else(|| "Missing opcode".to_string())?;
        let first_type = opcode % 1000 / 100;
        let second_type = opcode % 10_000 / 1000;
        let third_type = opcode % 100_000 / 10_000;
        let opcode = opcode % 100;

        match opcode {
            // No parameters
            99 => Ok(Operation::Stop),
            // One parameter
            3 | 4 | 9 => {
                let param = Parameter::new(
                    first_type,
                    self.get_position(self.cursor + 1)
                        .ok_or_else(|| "Missing I/O Arg".to_string())?,
                );
                Ok(if opcode == 3 {
                    Operation::Input { dest: param }
                } else if opcode == 4 {
                    Operation::Output { src: param }
                } else {
                    Operation::AdjustRelativeBase { arg: param }
                })
            }
            // Two parameters
            5 | 6 => {
                let arg = Parameter::new(
                    first_type,
                    self.get_position(self.cursor + 1)
                        .ok_or_else(|| "Missing arg".to_string())?,
                );
                let dest = Parameter::new(
                    second_type,
                    self.get_position(self.cursor + 2)
                        .ok_or_else(|| "Missing dest".to_string())?,
                );
                Ok(if opcode == 5 {
                    Operation::JumpTrue { arg, dest }
                } else {
                    Operation::JumpFalse { arg, dest }
                })
            }
            // Three parameters
            1 | 2 | 7 | 8 => {
                let lhs = Parameter::new(
                    first_type,
                    self.get_position(self.cursor + 1)
                        .ok_or_else(|| "Missing lhs".to_string())?,
                );
                let rhs = Parameter::new(
                    second_type,
                    self.get_position(self.cursor + 2)
                        .ok_or_else(|| "Missing rhs".to_string())?,
                );
                let dest = Parameter::new(
                    third_type,
                    self.get_position(self.cursor + 3)
                        .ok_or_else(|| "Missing dest".to_string())?,
                );
                Ok(if opcode == 1 {
                    Operation::Add { lhs, rhs, dest }
                } else if opcode == 2 {
                    Operation::Multiply { lhs, rhs, dest }
                } else if opcode == 7 {
                    Operation::LessThan { lhs, rhs, dest }
                // } else if opcode == 8 {
                } else {
                    Operation::Equals { lhs, rhs, dest }
                })
            }
            _ => Err(format!("Unknown opcode: {}", opcode)),
        }
    }

    fn forward(&mut self, amount: i64) {
        self.cursor += amount;
    }

    fn execute_current_op(&mut self) -> Result<Option<i64>, String> {
        let current_op = self.current_op()?;
        // eprintln!("Current op: {:?}, relative_base: {}", current_op, self.relative_base);
        match current_op {
            Operation::Add { lhs, rhs, dest } => {
                let result = self.get(lhs).ok_or_else(|| "lhs missing")?
                    + self.get(rhs).ok_or_else(|| "rhs missing")?;
                let dest = self
                    .get_mut(dest)
                    .ok_or_else(|| format!("add dest missing: {:?}", dest))?;
                *dest = result;
                self.forward(4);
                Ok(Some(result))
            }
            Operation::Multiply { lhs, rhs, dest } => {
                let result = self.get(lhs).ok_or_else(|| "lhs missing")?
                    * self.get(rhs).ok_or_else(|| "rhs missing")?;
                let dest = self
                    .get_mut(dest)
                    .ok_or_else(|| format!("multiply dest missing: {:?}", dest))?;
                *dest = result;
                self.forward(4);
                Ok(Some(result))
            }
            Operation::Input { dest } => {
                if let Some(result) = self.get_input() {
                    let dest = self
                        .get_mut(dest)
                        .ok_or_else(|| format!("input dest missing: {:?}", dest))?;
                    *dest = result;
                    self.forward(2);
                    self.state = MachineState::Running;
                    Ok(Some(result))
                } else {
                    self.state = MachineState::IOWait;
                    Ok(None)
                }
            }
            Operation::Output { src } => {
                let result = self.get(src).ok_or_else(|| "missing output".to_string())?;
                self.forward(2);
                self.output.push_back(result);
                self.last_output = Some(result);
                Ok(Some(result))
            }
            Operation::JumpTrue { arg, dest } => {
                let arg = self.get(arg).ok_or_else(|| "missing arg".to_string())?;
                let dest = self.get(dest).ok_or_else(|| "missing dest".to_string())?;
                if arg != 0 {
                    self.cursor = dest;
                    Ok(Some(dest))
                } else {
                    self.forward(3);
                    Ok(None)
                }
            }
            Operation::JumpFalse { arg, dest } => {
                let arg = self.get(arg).ok_or_else(|| "missing arg".to_string())?;
                let dest = self.get(dest).ok_or_else(|| "missing dest".to_string())?;
                if arg == 0 {
                    self.cursor = dest;
                    Ok(Some(dest))
                } else {
                    self.forward(3);
                    Ok(None)
                }
            }
            Operation::LessThan { lhs, rhs, dest } => {
                let lhs = self.get(lhs).ok_or_else(|| "lhs missing")?;
                let rhs = self.get(rhs).ok_or_else(|| "rhs missing")?;
                let dest = self
                    .get_mut(dest)
                    .ok_or_else(|| "dest missing".to_string())?;
                if lhs < rhs {
                    *dest = 1;
                } else {
                    *dest = 0;
                }
                let dest = *dest;
                self.forward(4);
                Ok(Some(dest))
            }
            Operation::Equals { lhs, rhs, dest } => {
                let lhs = self.get(lhs).ok_or_else(|| "lhs missing")?;
                let rhs = self.get(rhs).ok_or_else(|| "rhs missing")?;
                let dest = self
                    .get_mut(dest)
                    .ok_or_else(|| "dest missing".to_string())?;
                if lhs == rhs {
                    *dest = 1;
                } else {
                    *dest = 0;
                }
                let dest = *dest;
                self.forward(4);
                Ok(Some(dest))
            }
            Operation::AdjustRelativeBase { arg } => {
                self.relative_base += self
                    .get(arg)
                    .ok_or_else(|| format!("arg missing: {:?}", current_op))?;
                self.forward(2);
                Ok(Some(self.relative_base))
            }
            Operation::Stop => {
                self.state = MachineState::Halt;
                Ok(None)
            }
        }
    }
}

impl FromStr for IntcodeMachine {
    type Err = String;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let program = input
            .trim()
            .split(',')
            .map(|s| {
                s.parse::<i64>()
                    .map_err(|err| format!("error parsing! {:#?}", err))
            })
            .collect::<Result<Vec<i64>, String>>()?;
        Ok(Self {
            program,
            cursor: 0,
            relative_base: 0,
            input: VecDeque::default(),
            output: VecDeque::default(),
            last_output: None,
            state: MachineState::Initialized,
        })
    }
}

#[cfg(test)]
mod test {
    use super::{controller, IntcodeMachine, MachineState, Operation, Parameter::*};

    #[test]
    fn intcode_get() {
        let machine: IntcodeMachine = "1,0,0,0,99".parse().unwrap();
        assert_eq!(machine.get_position(0), Some(1));
        assert_eq!(machine.get_position(1), Some(0));
        assert_eq!(machine.get_position(2), Some(0));
        assert_eq!(machine.get_position(3), Some(0));
        assert_eq!(machine.get_position(4), Some(99));
        assert_eq!(machine.get_position(5), Some(0));
        assert_eq!(machine.get_position(-1), None);
    }

    #[test]
    fn intcode_current_op() {
        assert_eq!(
            "1,0,0,0,99"
                .parse::<IntcodeMachine>()
                .unwrap()
                .current_op()
                .unwrap(),
            Operation::Add {
                lhs: Position(0),
                rhs: Position(0),
                dest: Position(0)
            }
        );
        assert_eq!(
            "2,0,0,0,99"
                .parse::<IntcodeMachine>()
                .unwrap()
                .current_op()
                .unwrap(),
            Operation::Multiply {
                lhs: Position(0),
                rhs: Position(0),
                dest: Position(0)
            }
        );
        assert_eq!(
            "2,4,4,5,99,0"
                .parse::<IntcodeMachine>()
                .unwrap()
                .current_op()
                .unwrap(),
            Operation::Multiply {
                lhs: Position(4),
                rhs: Position(4),
                dest: Position(5)
            }
        );
        assert_eq!(
            "99".parse::<IntcodeMachine>()
                .unwrap()
                .current_op()
                .unwrap(),
            Operation::Stop
        );
        assert_eq!(
            "1101,0,0,0,99"
                .parse::<IntcodeMachine>()
                .unwrap()
                .current_op()
                .unwrap(),
            Operation::Add {
                lhs: Immediate(0),
                rhs: Immediate(0),
                dest: Position(0)
            }
        );
        assert_eq!(
            "1102,0,0,0,99"
                .parse::<IntcodeMachine>()
                .unwrap()
                .current_op()
                .unwrap(),
            Operation::Multiply {
                lhs: Immediate(0),
                rhs: Immediate(0),
                dest: Position(0)
            }
        );
        assert_eq!(
            "101,-2,5,0,99"
                .parse::<IntcodeMachine>()
                .unwrap()
                .current_op()
                .unwrap(),
            Operation::Add {
                lhs: Immediate(-2),
                rhs: Position(5),
                dest: Position(0)
            }
        );
        assert_eq!(
            "1002,3,1,0,99"
                .parse::<IntcodeMachine>()
                .unwrap()
                .current_op()
                .unwrap(),
            Operation::Multiply {
                lhs: Position(3),
                rhs: Immediate(1),
                dest: Position(0)
            }
        );
    }

    #[test]
    fn intcode_forward() {
        let mut machine = "2,4,4,5,99,0".parse::<IntcodeMachine>().unwrap();
        assert_eq!(
            machine.current_op().unwrap(),
            Operation::Multiply {
                lhs: Position(4),
                rhs: Position(4),
                dest: Position(5)
            }
        );

        machine.forward(4);
        assert_eq!(machine.current_op().unwrap(), Operation::Stop);

        machine.forward(4);
        assert!(machine.current_op().is_err());
    }

    #[test]
    fn intcode_execute_current_op_stop() {
        assert_eq!(
            "99".parse::<IntcodeMachine>()
                .unwrap()
                .execute_current_op()
                .unwrap(),
            None
        );
    }

    #[test]
    fn intcode_execute_current_op_add() {
        let mut machine = "1,0,0,0,99".parse::<IntcodeMachine>().unwrap();
        assert_eq!(machine.execute_current_op().unwrap(), Some(2));
        assert_eq!(machine.get_position(0).unwrap(), 2);
        assert_eq!(machine.execute_current_op().unwrap(), None);
    }

    #[test]
    fn intcode_execute_current_op_multiply() {
        let mut machine = "2,4,4,5,99,0".parse::<IntcodeMachine>().unwrap();
        assert_eq!(machine.state, MachineState::Initialized);
        assert_eq!(machine.execute_current_op().unwrap(), Some(9801));
        assert_eq!(machine.get_position(5).unwrap(), 9801);
        assert_eq!(machine.state, MachineState::Initialized); // shouldn't modify machine state
        assert_eq!(machine.execute_current_op().unwrap(), None);
        assert_eq!(machine.state, MachineState::Halt);
    }

    #[test]
    fn intcode_executes_current_op_immediate_multiply() {
        let mut machine = "1002,4,3,4,33".parse::<IntcodeMachine>().unwrap();
        assert_eq!(machine.state, MachineState::Initialized);
        assert_eq!(machine.execute_current_op().unwrap(), Some(99));
        assert_eq!(machine.state, MachineState::Initialized); // shouldn't modify machine state
        assert_eq!(machine.execute_current_op().unwrap(), None);
        assert_eq!(machine.state, MachineState::Halt);
    }

    #[test]
    fn intcode_run_jumps() {
        let mut machine = "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9"
            .parse::<IntcodeMachine>()
            .unwrap();
        machine.run(&[0]).unwrap();
        assert_eq!(machine.get_last_output().unwrap(), 0);

        let mut machine = "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9"
            .parse::<IntcodeMachine>()
            .unwrap();
        machine.run(&[1]).unwrap();
        assert_eq!(machine.get_last_output().unwrap(), 1);

        let mut machine = "3,3,1105,-1,9,1101,0,0,12,4,12,99,1"
            .parse::<IntcodeMachine>()
            .unwrap();
        machine.run(&[0]).unwrap();
        assert_eq!(machine.get_last_output().unwrap(), 0);

        let mut machine = "3,3,1105,-1,9,1101,0,0,12,4,12,99,1"
            .parse::<IntcodeMachine>()
            .unwrap();
        machine.run(&[1]).unwrap();
        assert_eq!(machine.get_last_output().unwrap(), 1);
    }

    #[test]
    fn intcode_run_comparison_position() {
        // Check for 8
        let mut machine = "3,9,8,9,10,9,4,9,99,-1,8"
            .parse::<IntcodeMachine>()
            .unwrap();
        machine.run(&[3]).unwrap();
        assert_eq!(machine.get_last_output().unwrap(), 0);

        let mut machine = "3,9,8,9,10,9,4,9,99,-1,8"
            .parse::<IntcodeMachine>()
            .unwrap();
        machine.run(&[8]).unwrap();
        assert_eq!(machine.get_last_output().unwrap(), 1);

        // Check for less than 8
        let mut machine = "3,9,7,9,10,9,4,9,99,-1,8"
            .parse::<IntcodeMachine>()
            .unwrap();
        machine.run(&[3]).unwrap();
        assert_eq!(machine.get_last_output().unwrap(), 1);

        let mut machine = "3,9,7,9,10,9,4,9,99,-1,8"
            .parse::<IntcodeMachine>()
            .unwrap();
        machine.run(&[8]).unwrap();
        assert_eq!(machine.get_last_output().unwrap(), 0);
    }

    #[test]
    fn intcode_run_comparison_immediate() {
        // Check for 8
        let mut machine = "3,3,1108,-1,8,3,4,3,99".parse::<IntcodeMachine>().unwrap();
        machine.run(&[3]).unwrap();
        assert_eq!(machine.get_last_output().unwrap(), 0);

        let mut machine = "3,3,1108,-1,8,3,4,3,99".parse::<IntcodeMachine>().unwrap();
        machine.run(&[8]).unwrap();
        assert_eq!(machine.get_last_output().unwrap(), 1);

        // Check for less than 8
        let mut machine = "3,3,1107,-1,8,3,4,3,99".parse::<IntcodeMachine>().unwrap();
        machine.run(&[3]).unwrap();
        assert_eq!(machine.get_last_output().unwrap(), 1);

        let mut machine = "3,3,1107,-1,8,3,4,3,99".parse::<IntcodeMachine>().unwrap();
        machine.run(&[8]).unwrap();
        assert_eq!(machine.get_last_output().unwrap(), 0);
    }

    #[test]
    fn intcode_run_controller_example_1() {
        let machine =
            "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5"
                .parse::<IntcodeMachine>()
                .unwrap();
        let tree = vec![
            ("a", machine.clone().with_input(&[9, 0]), "b"),
            ("b", machine.clone().with_input(&[8]), "c"),
            ("c", machine.clone().with_input(&[7]), "d"),
            ("d", machine.clone().with_input(&[6]), "e"),
            ("e", machine.clone().with_input(&[5]), "a"),
        ];
        let result = controller(tree, "e").unwrap();
        assert_eq!(result, 139629729);
    }

    #[test]
    fn intcode_run_controller_example_2() {
        let machine = "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10".parse::<IntcodeMachine>().unwrap();
        let tree = vec![
            ("a", machine.clone().with_input(&[9, 0]), "b"),
            ("b", machine.clone().with_input(&[7]), "c"),
            ("c", machine.clone().with_input(&[8]), "d"),
            ("d", machine.clone().with_input(&[5]), "e"),
            ("e", machine.clone().with_input(&[6]), "a"),
        ];
        let result = controller(tree, "e").unwrap();
        assert_eq!(result, 18216);
    }

    #[test]
    fn relative_mode_large_numbers_example_1() {
        let input = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99";
        let mut machine = input.parse::<IntcodeMachine>().unwrap();
        machine.run(&[]).unwrap();
        let result = machine
            .output
            .iter()
            .map(|v| v.to_string())
            .collect::<Vec<String>>()
            .join(",");
        assert_eq!(&result, input)
    }

    #[test]
    fn relative_mode_large_numbers_example_3() {
        let mut machine = "104,1125899906842624,99".parse::<IntcodeMachine>().unwrap();
        let expected = 1_125_899_906_842_624;
        machine.run(&[]).unwrap();
        assert_eq!(machine.get_last_output(), Some(expected));
    }
}
