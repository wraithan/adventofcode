pub fn solve_puzzle_part_1(input: &str) -> Result<i32, String> {
    Ok(input.len() as i32)
}

pub fn solve_puzzle_part_2(input: &str) -> Result<i32, String> {
    Ok(input.len() as i32)
}
