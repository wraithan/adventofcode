use std::ops::Sub;
use std::str::FromStr;

pub fn solve_puzzle_part_1(input: &str) -> Result<i32, String> {
    let mut wires: Vec<Wire> = input
        .lines()
        .filter(|l| !l.trim().is_empty())
        .map(|l| l.parse())
        .collect::<Result<_, _>>()?;
    if wires.len() != 2 {
        return Err("Wrong number of wires".to_string());
    }

    let origin = Vec2::new(0, 0);
    wires.iter_mut().for_each(|w| w.build_segments(origin));

    let wire_a = wires.get(0).unwrap();
    let wire_b = wires.get(1).unwrap();

    let mut closest_dist = std::i32::MAX;
    // let mut closest_intersection = None;
    for segment_a in &wire_a.segments {
        for segment_b in &wire_b.segments {
            if let Some(intersection) = segment_a.intersection(segment_b) {
                if intersection == origin {
                    continue;
                }
                let dist = intersection.manhattan_magnitude();
                if dist < closest_dist {
                    closest_dist = dist;
                    // eprintln!("Closest: {} ({:?}: {:?}, {:?})", dist, intersection, segment_a, segment_b);
                    // closest_intersection = Some(intersection);
                }
            }
        }
    }
    // closest_intersection.ok_or_else(|| "No Intersections?".to_string())
    Ok(closest_dist)
}

pub fn solve_puzzle_part_2(input: &str) -> Result<i32, String> {
    let mut wires: Vec<Wire> = input
        .lines()
        .filter(|l| !l.trim().is_empty())
        .map(|l| l.parse())
        .collect::<Result<_, _>>()?;
    if wires.len() != 2 {
        return Err("Wrong number of wires".to_string());
    }

    let origin = Vec2::new(0, 0);
    wires.iter_mut().for_each(|w| w.build_segments(origin));

    let wire_a = wires.get(0).unwrap();
    let wire_b = wires.get(1).unwrap();

    let mut shortest = std::i32::MAX;
    // let mut closest_intersection = None;
    for segment_a in &wire_a.segments {
        if segment_a.steps > shortest {
            continue;
        }
        for segment_b in &wire_b.segments {
            let base = segment_a.steps + segment_b.steps;
            if base > shortest {
                continue;
            }
            if let Some(intersection) = segment_a.intersection(segment_b) {
                if intersection == origin {
                    continue;
                }
                let length = {
                    let extra_a = (segment_a.start - intersection).manhattan_magnitude();
                    let extra_b = (segment_b.start - intersection).manhattan_magnitude();

                    base + extra_a + extra_b
                };
                if length < shortest {
                    shortest = length;
                    // eprintln!("Closest: {} ({:?}: {:?}, {:?})", dist, intersection, segment_a, segment_b);
                    // closest_intersection = Some(intersection);
                }
            }
        }
    }
    // closest_intersection.ok_or_else(|| "No Intersections?".to_string())
    Ok(shortest)
}

struct Wire {
    instructions: Vec<Instruction>,
    segments: Vec<LineSegment>,
}

impl Wire {
    fn build_segments(&mut self, origin: Vec2) {
        let mut last = origin;
        let mut steps = 0;
        for instruction in &self.instructions {
            let next = instruction.execute(last);

            self.segments.push(LineSegment {
                steps,
                start: last,
                end: next,
            });
            last = next;
            steps += instruction.amount();
        }
    }
}

impl FromStr for Wire {
    type Err = String;

    fn from_str(input: &str) -> Result<Wire, String> {
        let instructions: Vec<Instruction> = input
            .split(',')
            .filter(|i| !i.trim().is_empty())
            .map(|i| i.parse())
            .collect::<Result<_, _>>()?;

        let segments = vec![];

        Ok(Wire {
            instructions,
            segments,
        })
    }
}

#[derive(Debug)]
struct LineSegment {
    steps: i32,
    start: Vec2,
    end: Vec2,
}

impl LineSegment {
    fn intersection(&self, other: &LineSegment) -> Option<Vec2> {
        // eprintln!("lhs: {:?}, rhs: {:?}", self, other);
        let self_x_same = self.start.x == self.end.x;
        let self_y_same = self.start.y == self.end.y;
        let other_x_same = other.start.x == other.end.x;
        let other_y_same = other.start.y == other.end.y;
        // eprintln!("({}, {}), ({}, {})", self_x_same, self_y_same, other_x_same, other_y_same);
        let (stable_x, min_x, max_x) = if self_x_same {
            (
                self.start.x,
                other.start.x.min(other.end.x),
                other.start.x.max(other.end.x),
            )
        } else if other_x_same {
            (
                other.start.x,
                self.start.x.min(self.end.x),
                self.start.x.max(self.end.x),
            )
        // } else if self_y_same && other_y_same && self.start.y == other.start.y {
        // unimplemented!("Parallel on same line! (Y line)");
        } else {
            // parallel on different lines
            // eprintln!("parallel on different lines x");
            return None;
        };
        let (stable_y, min_y, max_y) = if self_y_same {
            (
                self.start.y,
                other.start.y.min(other.end.y),
                other.start.y.max(other.end.y),
            )
        } else if other_y_same {
            (
                other.start.y,
                self.start.y.min(self.end.y),
                self.start.y.max(self.end.y),
            )
        // } else if self_x_same && other_x_same && self.start.x == other.start.x {
        // unimplemented!("Parallel on same line! (X line)");
        } else {
            // parallel on different lines
            // eprintln!("parallel on different lines y");
            return None;
        };

        // eprintln!("stable: ({}, {}), min: ({}, {}), max: ({}, {})", stable_x, stable_y, min_x, min_y, max_x, max_y);
        if min_x <= stable_x && stable_x <= max_x && min_y <= stable_y && stable_y <= max_y {
            Some(Vec2::new(stable_x, stable_y))
        } else {
            None
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
struct Vec2 {
    x: i32,
    y: i32,
}

impl Vec2 {
    fn new(x: i32, y: i32) -> Self {
        Vec2 { x, y }
    }

    // fn magnitude2(&self) -> i32 {
    //     self.x * self.x + self.y * self.y
    // }

    fn manhattan_magnitude(self) -> i32 {
        // eprintln!("{:?}: {} + {}", self, self.x.abs(), self.y.abs());
        self.x.abs() + self.y.abs()
    }
}

impl Sub for Vec2 {
    type Output = Vec2;

    fn sub(self, other: Vec2) -> Vec2 {
        Vec2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
enum Instruction {
    Up(i32),
    Down(i32),
    Left(i32),
    Right(i32),
}

impl Instruction {
    fn execute(&self, mut from: Vec2) -> Vec2 {
        use Instruction::*;

        match self {
            Up(amount) => from.y += amount,
            Down(amount) => from.y -= amount,
            Left(amount) => from.x -= amount,
            Right(amount) => from.x += amount,
        };
        from
    }

    fn amount(&self) -> i32 {
        use Instruction::*;

        match self {
            Up(amount) | Down(amount) | Left(amount) | Right(amount) => *amount,
        }
    }
}

impl FromStr for Instruction {
    type Err = String;

    fn from_str(input: &str) -> Result<Instruction, String> {
        use Instruction::*;
        let mut chars = input.chars();
        let direction = chars
            .next()
            .ok_or_else(|| "Direction missing?".to_string())?;
        let amount = chars
            .as_str()
            .parse::<i32>()
            .map_err(|err| format!("Not a number? {:#?}", err))?;
        match direction {
            'U' => Ok(Up(amount)),
            'D' => Ok(Down(amount)),
            'L' => Ok(Left(amount)),
            'R' => Ok(Right(amount)),
            _ => Err(format!("Unknown direction: {}", direction)),
        }
    }
}

#[cfg(test)]
mod test_general {
    use super::{Instruction, LineSegment, Vec2};

    macro_rules! test_intersection {
        ($x1:expr, $y1:expr, $x2:expr, $y2:expr,
            $x3:expr, $y3:expr, $x4:expr, $y4:expr,
            $expected:expr) => {
            let segment_a = LineSegment {
                steps: 0,
                start: Vec2::new($x1, $y1),
                end: Vec2::new($x2, $y2),
            };
            let segment_b = LineSegment {
                steps: 0,
                start: Vec2::new($x3, $y3),
                end: Vec2::new($x4, $y4),
            };

            assert_eq!(segment_a.intersection(&segment_b), $expected)
        };
    }

    #[test]
    fn test_segment_intersect_not_intersecting() {
        let none_result: Option<Vec2> = None;

        #[rustfmt::skip]
        test_intersection!(
            0, 0, 1, 0,
            0, 1, 1, 1,
            none_result
        );
    }

    #[test]
    fn test_segment_intersect_intersecting_positive() {
        #[rustfmt::skip]
        test_intersection!(
            0, 1, 2, 1,
            1, 0, 1, 2,
            Some(Vec2::new(1, 1))
        );
    }

    #[test]
    fn test_segment_intersect_perpendicular_not_intersecting() {
        #[rustfmt::skip]
        test_intersection!(
            0, 0, 75, 0,
            66, 62, 66, 117,
            None
        );
    }

    #[test]
    fn test_intruction_parse() {
        assert_eq!("U233".parse(), Ok(Instruction::Up(233)));
        assert_eq!("D50".parse(), Ok(Instruction::Down(50)));
        assert_eq!("L824".parse(), Ok(Instruction::Left(824)));
        assert_eq!("R100".parse(), Ok(Instruction::Right(100)));
    }

    #[test]
    fn test_instruction_execute() {
        let start = Vec2::new(0, 0);
        assert_eq!(
            "U10".parse::<Instruction>().unwrap().execute(start),
            Vec2::new(0, 10)
        );
        assert_eq!(
            start,
            Vec2::new(0, 0),
            "Should copy the input and leave it unmodified"
        );

        assert_eq!(
            "L35"
                .parse::<Instruction>()
                .unwrap()
                .execute(Vec2::new(24, 14)),
            Vec2::new(-11, 14)
        );
    }
}

#[cfg(test)]
mod test_part_1 {
    use super::solve_puzzle_part_1;
    #[test]
    fn example_01() {
        let input = "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83\n";
        let expected = 159;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_02() {
        let input =
            "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7\n";
        let expected = 135;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }
}

#[cfg(test)]
mod test_part_2 {
    use super::solve_puzzle_part_2;
    #[test]
    fn example_01() {
        let input = "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83\n";
        let expected = 610;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_02() {
        let input =
            "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7\n";
        let expected = 410;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }
}
