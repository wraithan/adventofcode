pub fn solve_puzzle_part_1(input: &str) -> Result<i32, String> {
    input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            l.parse::<i32>()
                .map(mass_to_fuel)
                .map_err(|err| format!("Error parsing line: {:#?}", err))
        })
        .sum()
}

pub fn solve_puzzle_part_2(input: &str) -> Result<i32, String> {
    input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            l.parse::<i32>()
                .map(|mass| {
                    let mut current_mass = mass_to_fuel(mass);
                    let mut total_mass = 0;
                    while current_mass > 0 {
                        total_mass += current_mass;
                        current_mass = mass_to_fuel(current_mass);
                    }
                    total_mass
                })
                .map_err(|err| format!("Error parsing line: {:#?}", err))
        })
        .sum()
}

fn mass_to_fuel(mass: i32) -> i32 {
    (mass / 3) - 2
}

#[cfg(test)]
mod test_part_1 {
    use super::solve_puzzle_part_1;
    #[test]
    fn example_01() {
        let input = "12\n";
        let expected = 2;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_02() {
        let input = "14\n";
        let expected = 2;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_03() {
        let input = "1969\n";
        let expected = 654;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_04() {
        let input = "100756\n";
        let expected = 33583;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn combined() {
        let input = "12\n14\n1969\n100756\n";
        let expected = 2 + 2 + 654 + 33583;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected);
    }
}

#[cfg(test)]
mod test_part_2 {
    use super::solve_puzzle_part_2;
    #[test]
    fn example_01() {
        let input = "14\n";
        let expected = 2;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_02() {
        let input = "1969\n";
        let expected = 966;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_03() {
        let input = "100756\n";
        let expected = 50346;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn combined() {
        let input = "14\n1969\n100756\n";
        let expected = 2 + 966 + 50346;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected);
    }
}
