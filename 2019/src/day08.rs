pub fn solve_puzzle_part_1(input: &str) -> Result<i32, String> {
    let mut zeros = 0;
    let mut ones = 0;
    let mut twos = 0;
    let mut least_zeros = 6 * 25;
    let mut output = 0;
    for (i, val) in input.chars().enumerate() {
        match val {
            '0' => zeros += 1,
            '1' => ones += 1,
            '2' => twos += 1,
            _ => (),
        };
        if (i + 1) % (6 * 25) == 0 {
            if zeros < least_zeros {
                output = ones * twos;
                least_zeros = zeros;
            }
            zeros = 0;
            ones = 0;
            twos = 0;
        }
    }
    Ok(output)
}

pub fn solve_puzzle_part_2(input: &str) -> Result<i32, String> {
    let width = 25;
    let height = 6;
    let layer_length = width * height;
    // let layer_count = input.len() / layer_length;
    let mut image = Vec::with_capacity(layer_length);
    image.resize_with(layer_length, Default::default);

    for (i, val) in input.chars().enumerate() {
        let index = i % layer_length;
        if image[index] == None {
            match val {
                '0' => image[index] = Some(0),
                '1' => image[index] = Some(1),
                // Some(2) => twos += 1,
                _ => (),
            };
        }
    }
    // println!("{:?}", image);
    // for i in 0..layer_length {
    //     let pixel = if image[i].unwrap() == 1 {
    //         '*'
    //     } else {
    //         ' '
    //     };

    //     if (i + 1) % width == 0 {
    //         println!("{}", pixel)
    //     } else {
    //         print!("{}", pixel)
    //     }
    // }
    Ok(image.len() as i32)
}
