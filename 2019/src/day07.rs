use crate::intcode::{controller, IntcodeMachine};

pub fn solve_puzzle_part_1(input: &str) -> Result<i64, String> {
    let base_machine = input
        .parse::<IntcodeMachine>()
        .map_err(|err| format!("Couldn't parse program: {:#?}", err))?;
    let mut best_ouput = 0;
    for a in 0..=4 {
        for b in 0..=4 {
            if a == b {
                continue;
            }
            for c in 0..=4 {
                if a == c || b == c {
                    continue;
                }
                for d in 0..=4 {
                    if a == d || b == d || c == d {
                        continue;
                    }
                    for e in 0..=4 {
                        if a == e || b == e || c == e || d == e {
                            continue;
                        }
                        let phases = [a, b, c, d, e];
                        let mut amp_a = base_machine.clone();
                        let mut amp_b = base_machine.clone();
                        let mut amp_c = base_machine.clone();
                        let mut amp_d = base_machine.clone();
                        let mut amp_e = base_machine.clone();

                        amp_a.run(&[phases[0], 0])?;
                        let output_a = amp_a.get_last_output().unwrap();
                        amp_b.run(&[phases[1], output_a])?;
                        let output_b = amp_b.get_last_output().unwrap();
                        amp_c.run(&[phases[2], output_b])?;
                        let output_c = amp_c.get_last_output().unwrap();
                        amp_d.run(&[phases[3], output_c])?;
                        let output_d = amp_d.get_last_output().unwrap();
                        amp_e.run(&[phases[4], output_d])?;
                        let output_e = amp_e.get_last_output().unwrap();
                        if output_e > best_ouput {
                            // best_phases = Some(phases);
                            best_ouput = output_e;
                        }
                    }
                }
            }
        }
    }

    Ok(best_ouput)
}

pub fn solve_puzzle_part_2(input: &str) -> Result<i64, String> {
    let base_machine = input
        .parse::<IntcodeMachine>()
        .map_err(|err| format!("Couldn't parse program: {:#?}", err))?;
    let mut best_ouput = 0;
    for a in 5..=9 {
        for b in 5..=9 {
            if a == b {
                continue;
            }
            for c in 5..=9 {
                if a == c || b == c {
                    continue;
                }
                for d in 5..=9 {
                    if a == d || b == d || c == d {
                        continue;
                    }
                    for e in 5..=9 {
                        if a == e || b == e || c == e || d == e {
                            continue;
                        }
                        // eprintln!("testing: {}, {}, {}, {}, {}", a, b, c, d, e);
                        let output = controller(
                            vec![
                                ("a", base_machine.clone().with_input(&[a, 0]), "b"),
                                ("b", base_machine.clone().with_input(&[b]), "c"),
                                ("c", base_machine.clone().with_input(&[c]), "d"),
                                ("d", base_machine.clone().with_input(&[d]), "e"),
                                ("e", base_machine.clone().with_input(&[e]), "a"),
                            ],
                            "e",
                        )
                        .unwrap();

                        if output > best_ouput {
                            // best_phases = Some(phases);
                            best_ouput = output;
                        }
                    }
                }
            }
        }
    }

    Ok(best_ouput)
}

#[cfg(test)]
mod test_part_1 {
    use super::solve_puzzle_part_1;

    #[test]
    fn example_01() {
        let input = "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0\n";
        let expected = 43210;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn example_02() {
        let input = "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0\n";
        let expected = 54321;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn example_03() {
        let input = "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0\n";
        let expected = 65210;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected);
    }
}
