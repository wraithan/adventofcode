mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod intcode;

use std::time::{Duration, Instant};

macro_rules! expand_day {
    ($day_name:ident, $display_name:expr, $expected:expr) => {
        // Load input file
        let (input, load_time) = time_func(|| {
            let day_name = stringify!($day_name);
            let mut input_path = std::path::PathBuf::from(env!("CARGO_MANIFEST_DIR"));
            input_path.push("input");
            input_path.push(&format!("{}.txt", day_name));
            std::fs::read_to_string(input_path).expect("input text")
        });

        // Solve puzzles
        let (part1_answer, part1_time) =
            time_func(|| $day_name::solve_puzzle_part_1(&input).expect("don't crash"));

        let (part2_answer, part2_time) =
            time_func(|| $day_name::solve_puzzle_part_2(&input).expect("don't crash"));

        let part1_correct = if $expected.0 == part1_answer {
            '✓'
        } else {
            'X'
        };

        let part2_correct = if $expected.1 == part2_answer {
            '✓'
        } else {
            'X'
        };

        // Print result
        println!(
            "{} ({}.{:06})\n  Part 1 ({}.{:06}) ({}): {}\n  Part 2 ({}.{:06}) ({}): {}",
            $display_name,
            load_time.as_secs(),
            load_time.subsec_micros(),
            part1_time.as_secs(),
            part1_time.subsec_micros(),
            part1_correct,
            part1_answer,
            part2_time.as_secs(),
            part2_time.subsec_micros(),
            part2_correct,
            part2_answer,
        );
    };
}

fn time_func<F, T>(func: F) -> (T, Duration)
where
    F: FnOnce() -> T,
    T: Sized,
{
    let start = Instant::now();
    (func(), start.elapsed())
}

fn main() {
    let (_, total) = time_func(|| {
        expand_day!(day01, "Day 01", (3_270_338, 4_902_650));
        expand_day!(day02, "Day 02", (3_716_293, 6429));
        expand_day!(day03, "Day 03", (260, 15_612));
        expand_day!(day04, "Day 04", (1864, 1258));
        expand_day!(day05, "Day 05", (16_489_636, 9_386_583));
        expand_day!(day06, "Day 06", (151_345, 391));
        expand_day!(day07, "Day 07", (272_368, 19_741_286));
        // RCYKR is Day 8 Part 2 real answer
        expand_day!(day08, "Day 08", (1320, 150));
        expand_day!(day09, "Day 09", (2_890_527_621i64, 66772));
        expand_day!(day10, "Day 10", (0, 0));
    });
    println!(
        "Total time: {}.{:06}s",
        total.as_secs(),
        total.subsec_micros()
    );
}
