use std::collections::HashMap;

type OrbitHashMap<'a> = HashMap<&'a str, Vec<&'a str>>;

pub fn solve_puzzle_part_1(input: &str) -> Result<usize, String> {
    let map = build_orbit_map(input);

    Ok(count_orbits("COM", &map, 1))
}

pub fn solve_puzzle_part_2(input: &str) -> Result<usize, String> {
    let map = build_orbit_map(input);

    let mut you = vec!["COM"];
    let mut san = vec!["COM"];
    find_path_to(&mut you, &map, "YOU");
    find_path_to(&mut san, &map, "SAN");

    let mut diverge_at = 0;
    for (index, (y_body, s_body)) in you.iter().zip(&san).enumerate() {
        if y_body != s_body {
            diverge_at = index;
            break;
        }
    }

    Ok(you.len() - diverge_at + san.len() - diverge_at)
}

fn build_orbit_map(input: &str) -> OrbitHashMap {
    let mut map: OrbitHashMap = HashMap::default();
    input.lines().for_each(|l| {
        let mut parts = l.split(')');
        let parent = parts.next().expect("missing parent");
        let child = parts.next().expect("missing child");
        let parent = map.entry(parent).or_insert_with(|| vec![]);
        parent.push(child);
    });
    map
}

fn count_orbits(com: &str, map: &OrbitHashMap, depth: usize) -> usize {
    let children = map.get(com);
    if let Some(children) = children {
        let suborbits = children.len() * depth;
        suborbits
            + children
                .iter()
                .fold(0, |acc, c| acc + count_orbits(c, &map, depth + 1))
    } else {
        0
    }
}

fn find_path_to<'a>(path: &mut Vec<&'a str>, map: &OrbitHashMap<'a>, target: &str) -> bool {
    // eprintln!("Step: {:?}", path);
    if let Some(body) = map.get(path.last().unwrap()) {
        for child in body {
            if *child == target {
                return true;
            } else {
                path.push(child);
                if find_path_to(path, map, target) {
                    return true;
                }
            }
        }
    }
    path.pop();
    false
}

#[cfg(test)]
mod test_part_1 {
    use super::solve_puzzle_part_1;

    #[test]
    fn example_01() {
        let input = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\n";
        let expected = 42;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn simple_tests() {
        // COM - B
        let input = "COM)B\n";
        let expected = 1;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected);

        // COM - B - C
        let input = "COM)B\nB)C\n";
        let expected = 3;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected);

        //         G
        //        /
        // COM - B - C
        // B = 1
        // C = 2
        // G = 2
        let input = "COM)B\nB)C\nB)G\n";
        let expected = 5;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected);

        //         G - H
        //        /
        // COM - B - C - D
        // B = 1
        // C = 2
        // G = 2
        // H = 3
        // D = 3
        let input = "COM)B\nB)C\nB)G\nG)H\nC)D\n";
        let expected = 11;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected);
    }
}

#[cfg(test)]
mod test_part_2 {
    use super::solve_puzzle_part_2;

    #[test]
    fn example_01() {
        let input = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN\n";
        let expected = 4;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected);
    }
}
