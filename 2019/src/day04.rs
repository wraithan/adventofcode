pub fn solve_puzzle_part_1(input: &str) -> Result<usize, String> {
    let mut input = input.split('-').filter(|v| !v.trim().is_empty()).map(|v| {
        v.parse::<i32>()
            .map_err(|e| format!("Couldn't parse input: {}", e))
    });
    let start = input.next().unwrap()?;
    let end = input.next().unwrap()?;
    // let mut possibles = vec![];
    let mut count = 0;
    for d6 in 0..=9 {
        for d5 in d6..=9 {
            let d5_pair = d5 == d6;
            for d4 in d5..=9 {
                let d4_pair = d5_pair || d4 == d5;
                for d3 in d4..=9 {
                    let d3_pair = d4_pair || d3 == d4;
                    for d2 in d3..=9 {
                        let d2_pair = d3_pair || d2 == d3;
                        for d1 in d2..=9 {
                            if d2_pair || d1 == d2 {
                                let value = d6 * 100_000
                                    + d5 * 10_000
                                    + d4 * 1000
                                    + d3 * 100
                                    + d2 * 10
                                    + d1;
                                if value > end {
                                    break;
                                } else if value >= start {
                                    // _validate(start, end, value)?;
                                    // possibles.push(value);
                                    count += 1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    // println!("{} - {}", start, end);
    // println!("{:?}", possibles);
    Ok(count)
}

pub fn solve_puzzle_part_2(input: &str) -> Result<usize, String> {
    let mut input = input.split('-').filter(|v| !v.trim().is_empty()).map(|v| {
        v.parse::<i32>()
            .map_err(|e| format!("Couldn't parse input: {}", e))
    });
    let start = input.next().unwrap()?;
    let end = input.next().unwrap()?;
    // let mut possibles = vec![];
    let mut count = 0;
    for d6 in 0..=9 {
        for d5 in d6..=9 {
            let d5_pair = d5 == d6;
            for d4 in d5..=9 {
                let d4_pair = d4 == d5;

                for d3 in d4..=9 {
                    let d3_pair = d3 == d4;

                    for d2 in d3..=9 {
                        let d2_pair = d2 == d3;

                        for d1 in d2..=9 {
                            let d1_pair = d1 == d2;

                            if (d5_pair && !d4_pair)
                                || (!d5_pair && d4_pair && !d3_pair)
                                || (!d4_pair && d3_pair && !d2_pair)
                                || (!d3_pair && d2_pair && !d1_pair)
                                || (!d2_pair && d1_pair)
                            {
                                let value = d6 * 100_000
                                    + d5 * 10_000
                                    + d4 * 1000
                                    + d3 * 100
                                    + d2 * 10
                                    + d1;
                                if value > end {
                                    break;
                                } else if value >= start {
                                    // _validate(start, end, value)?;
                                    // possibles.push(value);
                                    count += 1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    // println!("{} - {}", start, end);
    // println!("{:?}", possibles);
    Ok(count)
}

fn _validate(start: i32, end: i32, value: i32) -> Result<bool, String> {
    // println!("{}", value);
    if value < start {
        return Err("too small".to_string());
    }

    if value > end {
        return Err("too big".to_string());
    }

    let digits: Vec<u32> = value
        .to_string()
        .chars()
        .map(|c| c.to_digit(10).unwrap())
        .collect();
    let mut has_pair = false;
    for pair in digits.windows(2) {
        let lhs = pair[0];
        let rhs = pair[1];
        if lhs > rhs {
            return Err("not descending".to_string());
        }
        has_pair = has_pair || lhs == rhs;
    }

    if !has_pair {
        return Ok(false);
        // return Err("no pair".to_string());
    }

    // Err("Invalid".to_string())
    Ok(true)
}
