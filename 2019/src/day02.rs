use crate::intcode::IntcodeMachine;

pub fn solve_puzzle_part_1(input: &str) -> Result<i64, String> {
    let mut machine: IntcodeMachine = input
        .parse()
        .map_err(|err| format!("Couldn't build intcode machine: {:#?}", err))?;
    {
        let program = machine.get_program_mut();
        program[1] = 12;
        program[2] = 2;
    }
    machine.run(&[])?;
    machine
        .get_position(0)
        .ok_or_else(|| "No program?".to_string())
}

pub fn solve_puzzle_part_2(input: &str) -> Result<i64, String> {
    let master_machine: IntcodeMachine = input
        .parse()
        .map_err(|err| format!("Couldn't build intcode machine: {:#?}", err))?;

    let mut machine = master_machine.clone();
    for i in 0..=99 {
        for j in 0..=99 {
            {
                let program = machine.get_program_mut();
                program.copy_from_slice(master_machine.get_program());
                program[1] = i;
                program[2] = j;
            }
            machine.reset();
            machine.run(&[])?;
            let result = machine
                .get_position(0)
                .ok_or_else(|| "No program?".to_string())?;
            if result == 19_690_720 {
                return Ok(i * 100 + j);
            }
        }
    }
    Err("No combo found".to_string())
}
