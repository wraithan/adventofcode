use crate::intcode::IntcodeMachine;

pub fn solve_puzzle_part_1(input: &str) -> Result<i64, String> {
    let mut machine: IntcodeMachine = input
        .parse()
        .map_err(|err| format!("Couldn't build intcode machine: {:#?}", err))?;

    machine.run(&[1])?;
    machine
        .get_last_output()
        .ok_or_else(|| "No program?".to_string())
}

pub fn solve_puzzle_part_2(input: &str) -> Result<i64, String> {
    let mut machine: IntcodeMachine = input
        .parse()
        .map_err(|err| format!("Couldn't build intcode machine: {:#?}", err))?;

    machine.run(&[5])?;
    machine
        .get_last_output()
        .ok_or_else(|| "No program?".to_string())
}
