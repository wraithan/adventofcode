pub fn solve_puzzle_part_1(input: &str) -> Result<u32, String> {
    let entries = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            l.parse::<u32>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))
        })
        .collect::<Result<Vec<u32>, String>>()?;

    for entry in &entries {
        let difference = 2020 - entry;
        if entries.contains(&difference) {
            return Ok(entry * difference);
        }
    }

    Err("No matching entries".into())
}

pub fn solve_puzzle_part_2(input: &str) -> Result<u32, String> {
    let entries = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            l.parse::<u32>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))
        })
        .collect::<Result<Vec<u32>, String>>()?;

    for entry_a in &entries {
        for entry_b in &entries {
            let together = entry_a + entry_b;
            if together >= 2020 {
                continue;
            }
            let difference = 2020 - together;
            if entries.contains(&difference) {
                return Ok(entry_a * entry_b * difference);
            }
        }
    }

    Err("No matching entries".into())
}

#[cfg(test)]
mod test_part_1 {
    use super::solve_puzzle_part_1;
    #[test]
    fn example_01() {
        let input = "1721\n979\n366\n299\n675\n1456\n";
        let expected = 514579;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }
}

#[cfg(test)]
mod test_part_2 {
    use super::solve_puzzle_part_2;
    #[test]
    fn example_01() {
        let input = "1721\n979\n366\n299\n675\n1456\n";
        let expected = 241_861_950;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }
}
