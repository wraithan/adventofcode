use std::str::FromStr;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Tile {
    Open,
    Tree,
}

struct Map {
    width: usize,
    // height: usize,
    tiles: Vec<Vec<Tile>>,
}

impl Map {
    fn get_tile(&self, x: usize, y: usize) -> Option<Tile> {
        let wrapped_x = x % self.width;

        self.tiles.get(y)?.get(wrapped_x).copied()
    }

    fn trees_on_angle(&self, step_x: usize, step_y: usize) -> usize {
        let mut x = 0;
        let mut y = 0;
        let mut trees = 0;
        while let Some(tile) = self.get_tile(x, y) {
            if tile == Tile::Tree {
                trees += 1;
            }
            x += step_x;
            y += step_y;
        }
        trees
    }
}

impl FromStr for Map {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let tiles: Vec<Vec<Tile>> = s
            .lines()
            .filter(|l| !l.is_empty())
            .map(|l| {
                l.chars()
                    .map(|c| match c {
                        '.' => Tile::Open,
                        '#' => Tile::Tree,
                        _ => unimplemented!("Unknown tile: {}", c),
                    })
                    .collect()
            })
            .collect();

        // let height = tiles.len();
        let width = tiles
            .get(0)
            .ok_or_else(|| "No first row?".to_string())?
            .len();

        Ok(Self {
            width,
            // height,
            tiles,
        })
    }
}

pub fn solve_puzzle_part_1(input: &str) -> Result<usize, String> {
    let map: Map = input.parse()?;

    Ok(map.trees_on_angle(3, 1))
}

pub fn solve_puzzle_part_2(input: &str) -> Result<u64, String> {
    let map: Map = input.parse()?;
    let angles = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];

    Ok(angles
        .into_iter()
        .map(|(step_x, step_y)| map.trees_on_angle(step_x, step_y) as u64)
        .product())
}

#[cfg(test)]
mod test_part_1 {
    use super::solve_puzzle_part_1;
    #[test]
    fn example_01() {
        let input = "..##.......\n#...#...#..\n.#....#..#.\n..#.#...#.#\n.#...##..#.\n..#.##.....\n.#.#.#....#\n.#........#\n#.##...#...\n#...##....#\n.#..#...#.#\n";
        let expected = 7;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }
}

#[cfg(test)]
mod test_part_2 {
    use super::solve_puzzle_part_2;

    #[test]
    fn example_01() {
        let input = "..##.......\n#...#...#..\n.#....#..#.\n..#.#...#.#\n.#...##..#.\n..#.##.....\n.#.#.#....#\n.#........#\n#.##...#...\n#...##....#\n.#..#...#.#\n";
        let expected = 336;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }
}
