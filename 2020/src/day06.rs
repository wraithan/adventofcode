use std::collections::HashSet;

pub fn solve_puzzle_part_1(input: &str) -> Result<usize, String> {
    Ok(input
        .lines()
        .fold(vec![vec![]], |mut acc, l| {
            if l.is_empty() {
                acc.push(vec![]);
            } else {
                acc.last_mut()
                    .expect("must always have an element")
                    .extend(l.split(' '))
            }

            acc
        })
        .into_iter()
        .fold(0, |acc, voters| {
            voters
                .iter()
                .fold(HashSet::new(), |mut set, votes| {
                    votes.chars().for_each(|c| {
                        set.insert(c);
                    });
                    set
                })
                .len()
                + acc
        }))
}

pub fn solve_puzzle_part_2(input: &str) -> Result<usize, String> {
    Ok(input
        .lines()
        .fold(vec![vec![]], |mut acc, l| {
            if l.is_empty() {
                acc.push(vec![]);
            } else {
                acc.last_mut()
                    .expect("must always have an element")
                    .extend(l.split(' '))
            }

            acc
        })
        .into_iter()
        .fold(0, |acc, voters| {
            voters
                .iter()
                .fold(
                    {
                        let mut base_set = HashSet::new();
                        for char in "abcdefghijklmnopqrstuvwxyz".chars() {
                            base_set.insert(char);
                        }
                        base_set
                    },
                    |set, votes| {
                        let mut new_set = HashSet::new();
                        votes.chars().for_each(|c| {
                            new_set.insert(c);
                        });
                        set.intersection(&new_set).cloned().collect()
                    },
                )
                .len()
                + acc
        }))
}

#[cfg(test)]
mod test_part_1 {
    use super::solve_puzzle_part_1;
    #[test]
    fn example_01() {
        let input = "abc\n";
        let expected = 3;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_02() {
        let input = "a\nb\nc\n";
        let expected = 3;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_03() {
        let input = "ab\nac\n";
        let expected = 3;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_04() {
        let input = "a\na\na\na\n";
        let expected = 1;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_05() {
        let input = "b\n";
        let expected = 1;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_all() {
        let input = "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb\n";
        let expected = 11;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }
}

#[cfg(test)]
mod test_part_2 {
    use super::solve_puzzle_part_2;
    #[test]
    fn example_01() {
        let input = "abc\n";
        let expected = 3;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_02() {
        let input = "a\nb\nc\n";
        let expected = 0;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_03() {
        let input = "ab\nac\n";
        let expected = 1;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_04() {
        let input = "a\na\na\na\n";
        let expected = 1;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_05() {
        let input = "b\n";
        let expected = 1;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_all() {
        let input = "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb\n";
        let expected = 6;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }
}
