use std::str::FromStr;

#[derive(Debug, PartialEq, Eq)]
struct Ticket {
    row: usize,
    column: usize,
}

impl FromStr for Ticket {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.len() != 10 {
            return Err(format!("string wrong length for a ticket: {}", s));
        }

        let (row, _, column, _) = s.chars().enumerate().fold(
            (0, 127, 0, 7),
            |(mut row_bottom, mut row_top, mut column_bottom, mut column_top), (i, c)| match i {
                0 | 1 | 2 | 3 | 4 | 5 => {
                    match c {
                        'F' => row_top -= ((row_top - row_bottom) + 1) / 2,
                        'B' => row_bottom += ((row_top - row_bottom) + 1) / 2,
                        _ => unreachable!(),
                    }
                    (row_bottom, row_top, column_bottom, column_top)
                }
                6 => match c {
                    'F' => (row_bottom, row_top, column_bottom, column_top),
                    'B' => (row_top, row_top, column_bottom, column_top),
                    _ => unreachable!(),
                },
                7 | 8 => {
                    match c {
                        'L' => column_top -= ((column_top - column_bottom) + 1) / 2,
                        'R' => column_bottom += ((column_top - column_bottom) + 1) / 2,
                        _ => unreachable!(),
                    }
                    (row_bottom, row_top, column_bottom, column_top)
                }
                9 => match c {
                    'L' => (row_bottom, row_top, column_bottom, column_top),
                    'R' => (row_bottom, row_top, column_top, column_top),
                    _ => unreachable!(),
                },
                _ => unreachable!(),
            },
        );

        Ok(Ticket { row, column })
    }
}

impl Ticket {
    fn seat_id(&self) -> usize {
        (self.row * 8) + self.column
    }
}

pub fn solve_puzzle_part_1(input: &str) -> Result<usize, String> {
    let tickets = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            l.parse::<Ticket>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))
        })
        .collect::<Result<Vec<Ticket>, String>>()?;
    tickets
        .into_iter()
        .max_by(|a, b| a.seat_id().cmp(&b.seat_id()))
        .ok_or_else(|| "No max seat id?".to_string())
        .map(|t| t.seat_id())
}

pub fn solve_puzzle_part_2(input: &str) -> Result<usize, String> {
    let tickets = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            l.parse::<Ticket>()
                .map_err(|err| format!("Error parsing line: {:#?}", err))
        })
        .collect::<Result<Vec<Ticket>, String>>()?;
    let mut seats: Vec<bool> = Vec::with_capacity(1023);
    seats.resize_with(1023, || false);
    let mut max_seat = 0;
    let mut min_seat = 1023;

    for ticket in tickets {
        let seat_id = ticket.seat_id();
        if seat_id > max_seat {
            max_seat = seat_id;
        }
        if seat_id < min_seat {
            min_seat = seat_id;
        }
        seats[seat_id] = true;
    }

    seats
        .into_iter()
        .enumerate()
        .skip(min_seat)
        .find(|(_i, v)| v == &false)
        .ok_or_else(|| "No missing seat?".to_string())
        .map(|(i, _v)| i)
}

#[cfg(test)]
mod test_part_1 {
    use super::{solve_puzzle_part_1, Ticket};
    #[test]
    fn example_01() {
        let input = "BFFFBBFRRR";
        let expected = Ticket { row: 70, column: 7 };
        let result: Ticket = input.parse().expect("Could not parse example input");
        assert_eq!(result, expected);
        assert_eq!(result.seat_id(), 567);
    }

    #[test]
    fn example_02() {
        let input = "FFFBBBFRRR";
        let expected = Ticket { row: 14, column: 7 };
        let result: Ticket = input.parse().expect("Could not parse example input");
        assert_eq!(result, expected);
        assert_eq!(result.seat_id(), 119);
    }

    #[test]
    fn example_03() {
        let input = "BBFFBBFRLL";
        let expected = Ticket {
            row: 102,
            column: 4,
        };
        let result: Ticket = input.parse().expect("Could not parse example input");
        assert_eq!(result, expected);
        assert_eq!(result.seat_id(), 820);
    }

    #[test]
    fn example_inline() {
        let input = "FBFBBFFRLR";
        let expected = Ticket { row: 44, column: 5 };
        let result: Ticket = input.parse().expect("Could not parse example input");
        assert_eq!(result, expected);
        assert_eq!(result.seat_id(), 357);
    }

    #[test]
    fn example_lowest() {
        let input = "FFFFFFFLLL";
        let expected = Ticket { row: 0, column: 0 };
        let result: Ticket = input.parse().expect("Could not parse example input");
        assert_eq!(result, expected);
        assert_eq!(result.seat_id(), 0);
    }

    #[test]
    fn example_highest() {
        let input = "BBBBBBBRRR";
        let expected = Ticket {
            row: 127,
            column: 7,
        };
        let result: Ticket = input.parse().expect("Could not parse example input");
        assert_eq!(result, expected);
        assert_eq!(result.seat_id(), 1023);
    }

    #[test]
    fn example_total() {
        let input = "BFFFBBFRRR\nFFFBBBFRRR\nBBFFBBFRLL\n";
        let expected = 820;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected);
    }
}
