mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;

use std::time::{Duration, Instant};

macro_rules! expand_day {
    ($day_name:ident, $display_name:expr, $expected:expr) => {
        // Load input file
        let (input, load_time) = time_func(|| {
            let day_name = stringify!($day_name);
            let mut input_path = std::path::PathBuf::from(env!("CARGO_MANIFEST_DIR"));
            input_path.push("input");
            input_path.push(&format!("{}.txt", day_name));
            std::fs::read_to_string(input_path).expect("input text")
        });

        // Solve puzzles
        let (part1_answer, part1_time) =
            time_func(|| $day_name::solve_puzzle_part_1(&input).expect("don't crash"));

        let (part2_answer, part2_time) =
            time_func(|| $day_name::solve_puzzle_part_2(&input).expect("don't crash"));

        let part1_correct = if $expected.0 == part1_answer {
            '✓'
        } else {
            'X'
        };

        let part2_correct = if $expected.1 == part2_answer {
            '✓'
        } else {
            'X'
        };

        // Print result
        println!(
            "{} (load time: {}.{:06}s)\n  Part 1 ({}.{:06}s) ({}): {}\n  Part 2 ({}.{:06}s) ({}): {}",
            $display_name,
            load_time.as_secs(),
            load_time.subsec_micros(),
            part1_time.as_secs(),
            part1_time.subsec_micros(),
            part1_correct,
            part1_answer,
            part2_time.as_secs(),
            part2_time.subsec_micros(),
            part2_correct,
            part2_answer,
        );
    };
}

fn time_func<F, T>(func: F) -> (T, Duration)
where
    F: FnOnce() -> T,
    T: Sized,
{
    let start = Instant::now();
    (func(), start.elapsed())
}

fn main() {
    let (_, total) = time_func(|| {
        expand_day!(day01, "Day 01", (437_931, 157_667_328));
        expand_day!(day02, "Day 02", (517, 284));
        expand_day!(day03, "Day 03", (159, 6_419_669_520u64));
        expand_day!(day04, "Day 04", (204, 179));
        expand_day!(day05, "Day 05", (928, 610));
        expand_day!(day06, "Day 06", (6885, 3550));
    });
    println!(
        "Total time: {}.{:06}s",
        total.as_secs(),
        total.subsec_micros()
    );
}
