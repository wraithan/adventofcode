#[derive(Default, Debug)]
struct Passport {
    birth_year: Option<Result<usize, String>>,
    issue_year: Option<Result<usize, String>>,
    expiration_year: Option<Result<usize, String>>,
    height: Option<Result<String, String>>,
    hair_color: Option<Result<String, String>>,
    eye_color: Option<Result<String, String>>,
    passport_id: Option<Result<usize, String>>,
    country_id: Option<Result<usize, String>>,
}

impl Passport {
    fn from_parts(passport_parts: Vec<&str>) -> Result<Self, String> {
        let mut passport = Passport::default();
        for passport_part in passport_parts {
            let mut parts = passport_part.split(':');
            let key = parts.next().ok_or_else(|| "key missing?".to_string())?;
            let value = parts.next().ok_or_else(|| "value missing?".to_string())?;

            match key {
                "byr" => {
                    passport.birth_year = Some(
                        value
                            .parse::<usize>()
                            .map_err(|e| {
                                format!("failed to parse birth year: {}:{}\n{}", key, value, e)
                            })
                            .and_then(|v| {
                                if v >= 1920 && v <= 2002 {
                                    Ok(v)
                                } else {
                                    Err(format!("{} is not a valid birth year", v))
                                }
                            }),
                    )
                }
                "iyr" => {
                    passport.issue_year = Some(
                        value
                            .parse::<usize>()
                            .map_err(|e| {
                                format!("failed to parse issue year: {}:{}\n{}", key, value, e)
                            })
                            .and_then(|v| {
                                if v >= 2010 && v <= 2020 {
                                    Ok(v)
                                } else {
                                    Err(format!("{} is not a valid issue year", v))
                                }
                            }),
                    )
                }
                "eyr" => {
                    passport.expiration_year = Some(
                        value
                            .parse::<usize>()
                            .map_err(|e| {
                                format!("failed to parse expiration year: {}:{}\n{}", key, value, e)
                            })
                            .and_then(|v| {
                                if v >= 2020 && v <= 2030 {
                                    Ok(v)
                                } else {
                                    Err(format!("{} is not a valid issue year", v))
                                }
                            }),
                    )
                }
                "hgt" => {
                    passport.height = Some({
                        let height_len = value.len();
                        if height_len < 4 && height_len < 6 {
                            return Err(format!("height value length wrong: {}", value));
                        }
                        if let Some(unit) = value.get(height_len - 2..height_len) {
                            match unit {
                                "cm" => {
                                    let height_cm =
                                        value[0..height_len - 2].parse::<usize>().map_err(|e| {
                                            format!("Failed to parse height cm: {}\n{}", value, e)
                                        })?;
                                    if height_cm < 150 || height_cm > 193 {
                                        Err(format!("height cm out of range: {}", height_cm))
                                    } else {
                                        Ok(height_cm.to_string())
                                    }
                                }
                                "in" => {
                                    let height_in =
                                        value[0..height_len - 2].parse::<usize>().map_err(|e| {
                                            format!("Failed to parse height in: {}\n{}", value, e)
                                        })?;
                                    if height_in < 59 || height_in > 76 {
                                        Err(format!("height in out of range: {}", height_in))
                                    } else {
                                        Ok(height_in.to_string())
                                    }
                                }
                                _ => Err(format!("Unknown height unit: {}", unit)),
                            }
                        } else {
                            Err(format!("height not long enough to have unit: {}", value))
                        }
                    })
                }
                "hcl" => {
                    passport.hair_color = Some({
                        if value.len() != 7 {
                            Err(format!("Hair color wrong length: {}", value.len()))
                        } else {
                            let mut value_chars = value.chars();
                            if let Some('#') = value_chars.next() {
                                if value_chars.any(|v| !v.is_digit(16)) {
                                    Err(format!("bad char in hair color: {}", value))
                                } else {
                                    Ok(value.to_string())
                                }
                            } else {
                                Err(format!("hair color missing hash: {}", value))
                            }
                        }
                    })
                }
                "ecl" => {
                    passport.eye_color = Some(match value {
                        "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth" => {
                            Ok(value.to_string())
                        }
                        _ => Err(format!("invalid eye color: {}", value)),
                    })
                }
                "pid" => {
                    passport.passport_id = Some(if value.len() == 9 {
                        value
                            .parse()
                            .map_err(|e| format!("failed to passport id: {}\n{}", value, e))
                    } else {
                        Err(format!("invalid pid {}", value))
                    })
                }
                "cid" => {
                    passport.country_id = Some(value.parse().map_err(|e| {
                        format!("failed to parse country id: {}:{}\n{}", key, value, e)
                    }))
                }
                _ => unimplemented!("unknown passport feature: {}:{}", key, value),
            }
        }
        Ok(passport)
    }

    fn has_all_non_country_fields(&self) -> bool {
        self.birth_year.is_some()
            && self.issue_year.is_some()
            && self.expiration_year.is_some()
            && self.height.is_some()
            && self.hair_color.is_some()
            && self.eye_color.is_some()
            && self.passport_id.is_some()
    }

    fn has_all_non_country_fields_and_are_valid(&self) -> bool {
        self.birth_year.as_ref().map_or(false, |v| v.is_ok())
            && self.issue_year.as_ref().map_or(false, |v| v.is_ok())
            && self.expiration_year.as_ref().map_or(false, |v| v.is_ok())
            && self.height.as_ref().map_or(false, |v| v.is_ok())
            && self.hair_color.as_ref().map_or(false, |v| v.is_ok())
            && self.eye_color.as_ref().map_or(false, |v| v.is_ok())
            && self.passport_id.as_ref().map_or(false, |v| v.is_ok())
    }
}

pub fn solve_puzzle_part_1(input: &str) -> Result<usize, String> {
    Ok(input
        .lines()
        .fold(vec![vec![]], |mut acc, l| {
            if l.is_empty() {
                acc.push(vec![]);
            } else {
                acc.last_mut()
                    .expect("must always have an element")
                    .extend(l.split(' '))
            }

            acc
        })
        .into_iter()
        .map(Passport::from_parts)
        .filter(|p| p.as_ref().map_or(false, |p| p.has_all_non_country_fields()))
        .count())
}

pub fn solve_puzzle_part_2(input: &str) -> Result<usize, String> {
    Ok(input
        .lines()
        .fold(vec![vec![]], |mut acc, l| {
            if l.is_empty() {
                acc.push(vec![]);
            } else {
                acc.last_mut()
                    .expect("must always have an element")
                    .extend(l.split(' '))
            }

            acc
        })
        .into_iter()
        .map(Passport::from_parts)
        .filter(|p| {
            p.as_ref()
                .map_or(false, |p| p.has_all_non_country_fields_and_are_valid())
        })
        .count())
}

#[cfg(test)]
mod test_part_1 {
    use super::solve_puzzle_part_1;
    #[test]
    fn example_01() {
        let input = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\nbyr:1937 iyr:2017 cid:147 hgt:183cm\n\niyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\nhcl:#cfa07d byr:1929\n\nhcl:#ae17e1 iyr:2013\neyr:2024\necl:brn pid:760753108 byr:1931\nhgt:179cm\n\nhcl:#cfa07d eyr:2025 pid:166559648\niyr:2011 ecl:brn hgt:59in\n";
        let expected = 2;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_02() {
        let input = "ecl:gry pid:8600a33327 eyr:2020 hcl:#fffffd\nbyr:1937 iyr:2017 cid:147 hgt:183cm\n\niyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\nhcl:#cfa07d byr:1929\n\nhcl:#ae17e1 iyr:2013\neyr:2024\necl:brn pid:760753108 byr:1931\nhgt:179cm\n\nhcl:#cfa07d eyr:2025 pid:166559648\niyr:2011 ecl:brn hgt:59in\n";
        let expected = 2;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }
}

#[cfg(test)]
mod test_part_2 {
    use super::solve_puzzle_part_2;

    #[test]
    fn invalid_example_01() {
        let input = "eyr:1972 cid:100\nhcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926\n";
        let expected = 0;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn invalid_example_02() {
        let input = "iyr:2019\nhcl:#602927 eyr:1967 hgt:170cm\necl:grn pid:012533040 byr:1946\n";
        let expected = 0;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn invalid_example_03() {
        let input =
            "hcl:dab227 iyr:2012\necl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277\n";
        let expected = 0;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn invalid_example_04() {
        let input = "hgt:59cm ecl:zzz\neyr:2038 hcl:74454a iyr:2023\npid:3556412378 byr:2007\n";
        let expected = 0;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn invalid_birth_year() {
        let byr_input_low =
            "byr:1919 iyr:2010 eyr:2021 hgt:158cm hcl:#b6652a ecl:amb pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(byr_input_low).unwrap(),
            0,
            "low birth year invalid"
        );
        let byr_input_high =
            "byr:2003 iyr:2010 eyr:2021 hgt:158cm hcl:#b6652a ecl:blu pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(byr_input_high).unwrap(),
            0,
            "high birth year invalid"
        );
    }
    #[test]
    fn invalid_issue_year() {
        let iyr_input_low =
            "byr:1920 iyr:2009 eyr:2021 hgt:59in hcl:#b6652a ecl:brn pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(iyr_input_low).unwrap(),
            0,
            "low issue year invalid"
        );
        let iyr_input_high =
            "byr:2002 iyr:2021 eyr:2021 hgt:76in hcl:#b6652a ecl:gry pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(iyr_input_high).unwrap(),
            0,
            "high issue year invalid"
        );
    }

    #[test]
    fn invalid_expiration_year() {
        let eyr_input_low =
            "byr:1920 iyr:2010 eyr:2019 hgt:150cm hcl:#b6652a ecl:grn pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(eyr_input_low).unwrap(),
            0,
            "low expiration year invalid"
        );
        let eyr_input_high =
            "byr:2002 iyr:2020 eyr:2031 hgt:193cm hcl:#b6652a ecl:hzl pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(eyr_input_high).unwrap(),
            0,
            "high expiration year invalid"
        );
    }
    #[test]
    fn invalid_height_unit() {
        let hgt_unit_invalid_input =
            "byr:2002 iyr:2020 eyr:2020 hgt:158yo hcl:#b6652a ecl:hzl pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(hgt_unit_invalid_input).unwrap(),
            0,
            "high height unit invalid"
        );
        let hgt_unit_missing_input =
            "byr:2002 iyr:2020 eyr:2030 hgt:158 hcl:#b6652a ecl:hzl pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(hgt_unit_missing_input).unwrap(),
            0,
            "high height unit missing"
        );
    }
    #[test]
    fn invalid_height_cm() {
        let hgt_cm_input_low =
            "byr:1920 iyr:2010 eyr:2019 hgt:149cm hcl:#b6652a ecl:grn pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(hgt_cm_input_low).unwrap(),
            0,
            "low height cm invalid"
        );
        let hgt_cm_input_high =
            "byr:2002 iyr:2020 eyr:2031 hgt:193cm hcl:#b6652a ecl:hzl pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(hgt_cm_input_high).unwrap(),
            0,
            "high height cm invalid"
        );
    }
    #[test]
    fn invalid_height_in() {
        let hgt_in_input_low =
            "byr:1920 iyr:2010 eyr:2020 hgt:58in hcl:#b6652a ecl:grn pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(hgt_in_input_low).unwrap(),
            0,
            "low height in invalid"
        );
        let hgt_in_input_high =
            "byr:2002 iyr:2020 eyr:2030 hgt:77in hcl:#b6652a ecl:hzl pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(hgt_in_input_high).unwrap(),
            0,
            "high height in invalid"
        );
    }

    #[test]
    fn invalid_hair_color() {
        let hcl_missing_hash_input =
            "byr:2002 iyr:2020 eyr:2020 hgt:158cm hcl:b6652a ecl:hzl pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(hcl_missing_hash_input).unwrap(),
            0,
            "hair color missing hash"
        );
        let hcl_missing_hash_right_length_input =
            "byr:2002 iyr:2020 eyr:2020 hgt:158cm hcl:b66a52a ecl:hzl pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(hcl_missing_hash_right_length_input).unwrap(),
            0,
            "hair color missing hash but has right length"
        );
        let hcl_bad_char_input =
            "byr:2002 iyr:2020 eyr:2030 hgt:158cm hcl:#b66k2a ecl:hzl pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(hcl_bad_char_input).unwrap(),
            0,
            "hair color bad char"
        );

        let hcl_too_many_char_input =
            "byr:2002 iyr:2020 eyr:2030 hgt:158cm hcl:#b66aa2a ecl:hzl pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(hcl_too_many_char_input).unwrap(),
            0,
            "hair color too long"
        );
        let hcl_not_enough_char_input =
            "byr:2002 iyr:2020 eyr:2030 hgt:158cm hcl:#b662a ecl:hzl pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(hcl_not_enough_char_input).unwrap(),
            0,
            "hair color too short"
        );
    }

    #[test]
    fn invalid_eye_color() {
        let ecl_invalid_input =
            "byr:1920 iyr:2010 eyr:2020 hgt:59in hcl:#b6652a ecl:gan pid:093154719\n";
        assert_eq!(
            solve_puzzle_part_2(ecl_invalid_input).unwrap(),
            0,
            "wrong eye color in invalid"
        );
    }

    #[test]
    fn invalid_passport_id() {
        let pid_short_input =
            "byr:1920 iyr:2010 eyr:2020 hgt:59in hcl:#b6652a ecl:grn pid:93154719\n";
        assert_eq!(
            solve_puzzle_part_2(pid_short_input).unwrap(),
            0,
            "passport id too short"
        );

        let pid_long_input =
            "byr:1920 iyr:2010 eyr:2020 hgt:59in hcl:#b6652a ecl:grn pid:0093154719\n";
        assert_eq!(
            solve_puzzle_part_2(pid_long_input).unwrap(),
            0,
            "passport id is too long"
        );

        let pid_not_decimal_input =
            "byr:1920 iyr:2010 eyr:2020 hgt:59in hcl:#b6652a ecl:grn pid:0931a4719\n";
        assert_eq!(
            solve_puzzle_part_2(pid_not_decimal_input).unwrap(),
            0,
            "passport id decimals only"
        );
    }

    #[test]
    fn valid_example_01() {
        let input = "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980\nhcl:#623a2f\n";
        let expected = 1;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn valid_example_02() {
        let input =
            "eyr:2029 ecl:blu cid:129 byr:1989\niyr:2014 pid:896056539 hcl:#a97842 hgt:165cm\n";
        let expected = 1;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn valid_example_03() {
        let input =
            "hcl:#888785\nhgt:164cm byr:2001 iyr:2015 cid:88\npid:545766238 ecl:hzl\neyr:2022\n";
        let expected = 1;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn valid_example_04() {
        let input = "iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719\n";
        let expected = 1;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn valid_example_all() {
        let input = "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980\nhcl:#623a2f\n\neyr:2029 ecl:blu cid:129 byr:1989\niyr:2014 pid:896056539 hcl:#a97842 hgt:165cm\n\nhcl:#888785\nhgt:164cm byr:2001 iyr:2015 cid:88\npid:545766238 ecl:hzl\neyr:2022\n\niyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719\n";
        let expected = 4;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }
}
