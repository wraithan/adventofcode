pub fn solve_puzzle_part_1(input: &str) -> Result<usize, String> {
    input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            let mut parts = l.split(' ');
            let range = parts.next().ok_or_else(|| "range missing".to_owned())?;

            let mut range_parts = range.split('-');
            let start = range_parts
                .next()
                .ok_or_else(|| "range start missing".to_owned())?
                .parse()
                .map_err(|err| format!("Error parsing range start: {:#?}", err))?;
            let end = range_parts
                .next()
                .ok_or_else(|| "range end missing".to_owned())?
                .parse()
                .map_err(|err| format!("Error parsing range end: {:#?}", err))?;

            let letter = parts
                .next()
                .ok_or_else(|| "letter missing".to_owned())?
                .chars()
                .next()
                .ok_or_else(|| "first letter missing".to_owned())?;
            let password = parts.next().ok_or_else(|| "password missing".to_owned())?;

            let count = password.matches(letter).count();
            if count >= start && count <= end {
                Ok(1)
            } else {
                Ok(0)
            }
        })
        .sum()
}

pub fn solve_puzzle_part_2(input: &str) -> Result<usize, String> {
    input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| {
            let mut parts = l.split(' ');
            let position = parts.next().ok_or_else(|| "position missing".to_owned())?;

            let mut position_parts = position.split('-');
            let first: usize = position_parts
                .next()
                .ok_or_else(|| "position first missing".to_owned())?
                .parse()
                .map_err(|err| format!("Error parsing position first: {:#?}", err))?;
            let second: usize = position_parts
                .next()
                .ok_or_else(|| "position second missing".to_owned())?
                .parse()
                .map_err(|err| format!("Error parsing position second: {:#?}", err))?;

            let letter = parts
                .next()
                .ok_or_else(|| "letter missing".to_owned())?
                .chars()
                .next()
                .ok_or_else(|| "first letter missing".to_owned())?;
            let password = parts.next().ok_or_else(|| "password missing".to_owned())?;

            let first_match = password
                .chars()
                .nth(first - 1)
                .map(|c| c == letter)
                .unwrap_or(false);
            let second_match = password
                .chars()
                .nth(second - 1)
                .map(|c| c == letter)
                .unwrap_or(false);
            if (first_match && !second_match) || (!first_match && second_match) {
                Ok(1)
            } else {
                Ok(0)
            }
        })
        .sum()
}

#[cfg(test)]
mod test_part_1 {
    use super::solve_puzzle_part_1;
    #[test]
    fn example_01() {
        let input = "1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc\n";
        let expected = 2;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_01_a() {
        let input = "1-3 a: abcde";
        let expected = 1;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_01_b() {
        let input = "1-3 b: cdefg";
        let expected = 0;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_01_c() {
        let input = "2-9 c: ccccccccc";
        let expected = 1;
        let result = solve_puzzle_part_1(input).unwrap();
        assert_eq!(result, expected)
    }
}

#[cfg(test)]
mod test_part_2 {
    use super::solve_puzzle_part_2;
    #[test]
    fn example_01() {
        let input = "1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc\n";
        let expected = 1;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_01_a() {
        let input = "1-3 a: abcde";
        let expected = 1;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_01_b() {
        let input = "1-3 b: cdefg";
        let expected = 0;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn example_01_c() {
        let input = "2-9 c: ccccccccc";
        let expected = 0;
        let result = solve_puzzle_part_2(input).unwrap();
        assert_eq!(result, expected)
    }
}
